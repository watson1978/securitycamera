#import <Photos/Photos.h>
#import <Foundation/Foundation.h>

@interface Album : NSObject

+ (PHAssetCollection*)getSecurityCameraAlbum;
+ (void)createSecurityCameraAlbum;
+ (void)saveImageToAlbum:(UIImage*)image;

@end
