#import "Album.h"

static NSString *kAlbumTitle = @"Security Camera";

@implementation Album

+ (PHAssetCollection*)getSecurityCameraAlbum
{
    PHFetchResult *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAny options:nil];
    __block PHAssetCollection *securityCameraAlbum;

    // Find an album.
    [result enumerateObjectsUsingBlock:^(PHAssetCollection *album, NSUInteger idx, BOOL *stop) {
        if ([album.localizedTitle isEqualToString:kAlbumTitle]) {
            securityCameraAlbum = album;
            *stop = YES;
        }
    }];

    return securityCameraAlbum;
}

+ (void)createSecurityCameraAlbum
{
    PHAssetCollection *album = [self getSecurityCameraAlbum];
    if (album == nil) {
        // If not found an album, it create new album for
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *createAlbumRequest = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:kAlbumTitle];
            [createAlbumRequest placeholderForCreatedAssetCollection];
        } completionHandler:^(BOOL success, NSError *error) {
            NSLog(@"Finished to create album. %@", (success ? @"Success" : error));
        }];
    }
}

+ (void)saveImageToAlbum:(UIImage*)image
{
    PHAssetCollection *album = [self getSecurityCameraAlbum];
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *createAssetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:album];
        PHObjectPlaceholder *placeHolder = [createAssetRequest placeholderForCreatedAsset];
        [albumChangeRequest addAssets:@[placeHolder]];
    } completionHandler:^(BOOL success, NSError *error) {
        NSLog(@"Finished adding asset. %@", (success ? @"Success" : error));
    }];
}

@end
