#import <UIKit/UIKit.h>
#import "Album.h"

@interface AlbumViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>
{
    NSMutableArray *assets;

    __weak IBOutlet UICollectionView *collectionView;
}

@end
