#import "AlbumViewController.h"

@interface AlbumViewController ()

@end

@implementation AlbumViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    collectionView.delegate = self;
    collectionView.dataSource = self;

    PHAssetCollection *assetCollection = [Album getSecurityCameraAlbum];
    PHFetchResult *assetsFetchResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:nil];
    assets = NSMutableArray.new;
    [assetsFetchResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL *stop) {
        [assets addObject:asset];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [assets count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    UIImageView *imageView = [[cell contentView] viewWithTag:1];

    PHImageManager *manager = [PHImageManager defaultManager];
    [manager requestImageForAsset:[assets objectAtIndex:indexPath.row]
                       targetSize:CGSizeMake(180, 180)
                      contentMode:PHImageContentModeAspectFit options:nil
                    resultHandler:^(UIImage *image, NSDictionary *info) {
        [imageView setImage:image];
    }];

    return cell;
}

- (IBAction)dismiss:(id)sender
{
     [self dismissViewControllerAnimated:YES completion:nil];
}

@end
