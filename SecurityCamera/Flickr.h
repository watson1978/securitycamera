#import <Foundation/Foundation.h>


@interface Flickr : NSObject

- (BOOL)isAuthorized;
- (void)checkAuthorization:(void (^)(NSError *err))completion;
- (void)logout;
- (void)doAuthentication;
- (void)upload:(UIImage*)image;

@end
