#import <UIKit/UIKit.h>
#import "Flickr.h"
#import "FlickrKit.h"

// FlickrKit is defining "namespace" variable as public.
// The variable conflict with C++ regsterd `namespace` keyword and causes build error.
// So, wrapper methods are defined in here and uses it in "ViewController.mm"

static NSString *callbackURLString = @"securitycamera://auth";

@interface Flickr ()
@property (nonatomic, retain) FKDUNetworkOperation *checkAuthOp;
@property (nonatomic, retain) FKDUNetworkOperation *authOp;
@property (nonatomic, retain) FKImageUploadNetworkOperation *uploadOp;
@end

@implementation Flickr

- (BOOL)isAuthorized
{
    return [FlickrKit sharedFlickrKit].isAuthorized;
}

- (void)logout
{
    [[FlickrKit sharedFlickrKit] logout];
}

- (void)checkAuthorization:(void (^)(NSError *err))completion
{
    self.checkAuthOp = [[FlickrKit sharedFlickrKit] checkAuthorizationOnCompletion:^(NSString *userName, NSString *userId, NSString *fullName, NSError *error) {
        completion(error);
    }];

}

- (void)doAuthentication
{
    self.authOp = [[FlickrKit sharedFlickrKit] beginAuthWithCallbackURL:[NSURL URLWithString:callbackURLString] permission:FKPermissionDelete completion:^(NSURL *flickrLoginPageURL, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [[UIApplication sharedApplication] openURL:flickrLoginPageURL options:@{} completionHandler:nil];
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        });
    }];

}

- (void)upload:(UIImage*)image
{
    // Upload photo as private.
    self.uploadOp = [[FlickrKit sharedFlickrKit] uploadImage:image args:@{@"is_public": @"0", @"is_friend": @"0", @"is_family": @"0", @"hidden": @"2"} completion:^(NSString *imageID, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                // oops!
            }
            else {
                // Image is now in flickr!
            }
        });
    }];
}

@end
