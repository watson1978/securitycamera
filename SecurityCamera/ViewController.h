#import <UIKit/UIKit.h>

#import <opencv2/opencv.hpp>
#import <opencv2/videoio/cap_ios.h>
#import <opencv2/imgcodecs/ios.h>
#import "Flickr.h"

using namespace cv;

@interface ViewController : UIViewController<CvVideoCameraDelegate>
{
    Flickr *flickr;

    CvVideoCamera* videoCamera;
    Mat fgMaskMOG2;
    Ptr<BackgroundSubtractor> pMOG2;
    int frameCount;
    bool recording;

    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UIButton *recordButton;
    __weak IBOutlet UIButton *loginButton;
}

@end

