#import "ViewController.h"
#import "Album.h"

#define FPS 30
#define DETECT_MOVING_PER_FPS (FPS * 5) // Detect moving object per 5 sec
#define THRESHOLD_TO_SAVE_IMAGE 0.1

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

   	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userAuthenticateSucceeded:) name:@"UserAuthenticateSucceeded" object:nil];
    flickr = [Flickr new];
    [flickr checkAuthorization:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (!error) {
                [loginButton setTitle:@"Log out" forState:UIControlStateNormal];
            }
            else {
                [loginButton setTitle:@"Log in" forState:UIControlStateNormal];
            }
        });
    }];

    videoCamera = [[CvVideoCamera alloc] initWithParentView:imageView];
    videoCamera.delegate = self;
    videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
    videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset352x288;
    videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    videoCamera.defaultFPS = FPS;
    videoCamera.grayscaleMode = NO;
    recording = false;
    frameCount = 0;

    pMOG2 = createBackgroundSubtractorMOG2();
}

- (void)userAuthenticateSucceeded:(NSNotification *)notification
{
    [loginButton setTitle:@"Log out" forState:UIControlStateNormal];
}

- (void)processImage:(Mat&)image
{
    pMOG2->apply(image, fgMaskMOG2);

    frameCount++;
    if (frameCount % DETECT_MOVING_PER_FPS != 0) {
        return;
    }
    frameCount = 0;

    Mat binaryImage;
    cv::threshold(fgMaskMOG2, binaryImage, 0, 255.0, CV_THRESH_OTSU);

    int totalNumberOfPixels = binaryImage.rows * binaryImage.cols;
    int zeroPixels = totalNumberOfPixels - cv::countNonZero(binaryImage);
    double rate = ((double)(totalNumberOfPixels - zeroPixels)) / (double)totalNumberOfPixels;
    NSLog(@"rate = %lf", rate);

    if (rate > THRESHOLD_TO_SAVE_IMAGE) {
        Mat saveImage;
        cvtColor(image, saveImage, COLOR_BGR2BGRA);
        UIImage *image = MatToUIImage(saveImage);
        [Album saveImageToAlbum:image];
        if ([flickr isAuthorized]) {
            [flickr upload:image];
        }

    }

}

- (IBAction)actionRecord:(id)sender
{
    if (recording) {
        [videoCamera stop];
        [recordButton setImage:[UIImage imageNamed:@"Start"] forState:UIControlStateNormal];
        recording = false;
    }
    else {
        [videoCamera start];
        [recordButton setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
        recording = true;
    }
}

- (IBAction)actionSwitchCamera:(id)sender
{
    [videoCamera switchCameras];
}

- (IBAction)actionLogIn:(id)sender
{
    if ([flickr isAuthorized]) {
        [flickr logout];
        [loginButton setTitle:@"Log in" forState:UIControlStateNormal];
        return;
    }

    [flickr doAuthentication];
}

@end
